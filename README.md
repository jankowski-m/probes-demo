# Why write this blog post?

As part of the Kubernetes Platform team, I and my colleagues are often asked to answer questions like
* What is wrong with my container?
* Why does this service returns 500 HTTP errors so often?
* Why is this service so unreliable?
* Is there something wrong with the cluster? My service doesn't work...

Usually, it's not the cluster or service's helm configuration that's the problem. Usually, the problem is within the application itself. We tried two options to handle those requests for help:
* Passive-aggressive attitude: "We're very busy with other issues, please try to fix it yourself. You can escalate to yours/ours manager if you think we should help you immediately, but we are very busy so we will still not work on it.."
* *Servant* attitude: "Let me check that for you and come back with some answers.."

From my experience, the first one gets you nowhere in the long term, and only antagonizes people against you and your team. But, the latter doesn't scale well. It's very easy to be drowning in those types of requests. So what else can we do? 🤔

With our team, we decided that we would write a blog post every time someone asks us for help. When someone else comes back with a similar question, we would refer them to that blog post.

This way we want to achieve:
* build an extensive knowledge base 🤓
* make it easier for other devs to solve their questions by themselves 🤓
* gain trust of other teams, who are not yet onboarded on our platform 💪
* document/automate those requests away 😃🪄
