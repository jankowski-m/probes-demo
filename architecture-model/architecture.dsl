workspace {

    model {
        user = person "User"
        softwareSystem = softwareSystem "Kubernetes Cluster" "EKS cluster"{ 
          webapp = container "Frontend Service" 
          backend = container "Backend Service" 
          anotherapp = container "Another Service" 
          anotherapp2 = container "Another Service 2" 
          user -> webapp "Uses"
          webapp -> backend "Communicates with" 
          anotherapp -> backend "Communicates with"
          anotherapp -> anotherapp2 "Communicates with"  
        }
    }

    views {
        systemContext softwareSystem {
            include *
            autolayout lr
        }

        container softwareSystem "Context" "high overview"{
            include *
            autolayout lr
        }

    }

}
