---
title: 'How to use the liveness and readiness HTTP probes in Kubernetes'
subtitle: 'Fixing reliability issue without touching the application code'
publishstatus: draft
author: Michał Jankowski
date: 2021-07-09T09:30:00+0200
keywords: [kubernetes, probes, liveness, readiness,reliability]
lang: en-US
abstract: |
   Short description of a missconfigured readines and liveness probes that resulted in decreased reliability of the service.
email: pl.jankowskimichal@gmail.com
---

# How to use the liveness and readiness HTTP probes in Kubernetes
Fixing reliability issue without touching the application code

## Problem introduction
I'm part of the Platform team. Our responsibility is to provide a Kubernetes platform for other teams within the organization. We  ensure that platform is stable, utilized and reliable. Recently we got a request about an interesting issue. One of the fronted services hosted on our platform was reporting a lot of errors. The majority of those errors fell in one of the two patterns:

` Failed to serve request: GET /fake-url/-**- FetchError: network timeout at: -**- `

` Failed to serve request: GET /fake-url/*/-**- FetchError: request to -**- failed, reason: -**- `

Requests to the Backend service were failing. On a high level of [C2 diagram](https://c4model.com/#ContainerDiagram) the simplified sytem could look like this:

![Services hosted on Kubernetes Platform](https://gitlab.com/jankowski-m/probes-demo/-/raw/master/architecture-model/diagrams/structurizr-Context.png)


The development team suspected an issue with  the cluster configuration. Their assumption was that the cluster itself is not able to handle bigger load we have seen for few weeks. They asked us to improve the reliability of connections between the two services.

## Initial investigation


At first, we were suspecting an issue with the nginx controller configuration, but we didn't spot any correlation between the number of open connections and the error log counter.
![Frontend Service error logs counter vs Nginx ingress controller active connections counter](https://gitlab.com/jankowski-m/probes-demo/-/raw/master/pictures/error%20logs%20vs%20nginx%20controller%20connections.png)

 We jumped to the conclusion that the issue must lie within the backend service source code or within its configuration a.k.a Helm chart. Not knowing much about the application architecture, or its logic we focused on the helm chart. 

Our initial assumption was simple: We have a bigger load, we need more instances! We thought that a minimum of 5 replicas is not enough. We set minimum replicas to 10 to see if it helps. It did help... but not enough to call it a day and move on. 
 We should give it more resources (scale-up) and that will solve it, right? Looking at the graph of CPU/Memory utilization for our backend service Deployment made it obvious that this is not a problem.
![CPU usage](https://gitlab.com/jankowski-m/probes-demo/-/raw/master/pictures/cpu-usage.png)

 The entire Deployment is always within the requested resources even when under load. 

We also noticed more errors were reported when backend containers were reported as unavailable. In the graph below you can see that for yourself. You will also notice that some containers are unavailable during midnights but almost no errors can be seen in the Frontend service error logs counter. This is because on a nightly basis the backend service is queried by yet another service.

![Metrics comparison before changes](https://gitlab.com/jankowski-m/probes-demo/-/raw/master/pictures/blog-before.png)


We inspected the container specification further. But before we look at the configuration snippet, let's have a quick recap on Kubernetes Probes. We noticed a very interesting (mis)configuration that may not be obvious without it.

## Kubernetes Probes 

[The first paragraph of the documentation](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/) can be summed up in the following way. 

Liveness, readiness, and startup probes are there to inform the kublet about the health of the container for which they were defined.

A Startup Probe is used when starting the container to disable the other two probes and make sure they don't affect the application startup. A Startup Probe is useful with slowly starting containers, and is out of scope for our story here. 

A Liveness Probe informs the kublet when to restart the container.  

Readiness Probe determines whether a specific Pod is ready to accept the incoming requests or not. If not the Service load balancer will send it to another backend Pod.  

All probes have the same defaults controls such as: 
- period seconds (default value 10s) 
- failureThreshold (default value 3) 
- successThreshold (default value 1) 

After we refreshed our knowledge, let's have a look at that configuration snippet of the backend service container. 

```
cat deployment.yaml
[...]
        ports:
        - name: http
          containerPort: {{ .Values.containerPort }}
          protocol: TCP
        livenessProbe:
          httpGet:
            path: /status
            port: http
          initialDelaySeconds: 5
          timeoutSeconds: 5
        readinessProbe:
          httpGet:
            path: /status
            port: http
          initialDelaySeconds: 5
          timeoutSeconds: 5
[...]
cat service.yaml
[...]
spec:
  type: {{ .Values.service.type }}
  ports:
    - port: {{ .Values.service.port }}
      targetPort: {{ .Values.containerPort }}
      protocol: TCP
      name: http
[...]
```

As you can see, Readiness Probe and Liveness Probe are configured with exactly the same settings. Checks are performed using the same port(webserver) as the customer requests. Within the same (default) period of 10 seconds!


After we realized that, we formed a working hypothesis. Connections with the frontend service are kept open for a long time. If we assume that the service is performing some CPU-intensive tasks, and [blocking the Event Loop](https://nodejs.org/en/docs/guides/dont-block-the-event-loop/), any new connection will be dropped. This also includes the Probes requests. 
Then after 30 seconds, the container is tagged "Unready" by failed Readiness Probe requests. But it is also restarted by the failed Liveness Probe requests at the same time! 
During those 30 seconds, kube-proxy was directing traffic to that allegedly "Ready" container. 
Frontend service would report an error when a connection was not opened since backend service wasn't actually "Ready" to handle them. It would also report another error when connections that were already open got terminated when the container restarted.

We wanted to treat the backend service as a BlackBox so we asked the development team to verify our assumption about the blocked event loop. In the meantime, we decided to create a new configuration for both probes.

```
cat deployment.yaml
[...]
        ports:
        - name: http
          containerPort: {{ .Values.containerPort }}
          protocol: TCP
        livenessProbe:
          httpGet:
            path: /status
            port: http
          initialDelaySeconds: 5
          timeoutSeconds: 5
          periodSeconds: 60
          failureThreshold: 3
          successThreshold: 1
        readinessProbe:
          httpGet:
            path: /status
            port: http
          initialDelaySeconds: 5
          timeoutSeconds: 5
          periodSeconds: 10
          failureThreshold: 1
          successThreshold: 1
[...]
```
As you can see, we did three things:
- extend the `periodSeconds` of the Liveness Probe to 60 seconds;
- decreased the `failureThreshold` of the Readiness Probe to 1;
- mention the default configuration values explicitly to cut the cognitive load required to understand the full probe configuration.

So now, when our backend Pods are under heavy load, first they will be tagged "Unready" after 10 seconds. The Kube Proxy will direct the traffic to a different, "Ready", Pod. The unready Pod will get 3 minutes from the Liveness Probe to serve the responses within already open connections. Within that time it should start answering both probes again, otherwise, Probe will restart the container.

We've deployed our changes on Friday 2nd in the afternoon. Our service is mostly used during working hours so we had to wait till Monday to see if our changes improved the situation:

![Backend Service Reliability before and after the changes](https://gitlab.com/jankowski-m/probes-demo/-/raw/master/pictures/overall.png)

As you can see we didn't completely eliminate the errors. But it is quite visible that the reliability of the Backend service improved. The error counter went down, while the Backend service CPU usage stayed the same.

## Conclusions

Platform Engineers may not always know the exact reason for the application failure. But making sure that the service has a reasonable configuration improves its reliability.  Kubernetes provide many easy ways to control the lifecycle of the application. 
  
In this short blog post I tried to show that one should pay attention to a proper configuration of the Kubernetes Probes. Consequences of missconfiguration may surface when you least expect it. So, now please go back to your codebase and inspect your Kubernetes Probes configuration. Thanks for reading!
