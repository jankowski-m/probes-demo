#!/bin/bash
kind create cluster --name probes --config ./cluster.yaml
kubectl cluster-info --context kind-probes

kubectl apply -f deployment-kuard.yaml
kubectl apply -f svc-kuard.yaml

kubectl port-forward svc/kuard  8081:8080
